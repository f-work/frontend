import axios from "axios";

let baseURL = "http://localhost:8000";
if (process.env.NODE_ENV === "production") {
  baseURL = "https://student-tutor-server.herokuapp.com";
}

const api = axios.create({
  baseURL,
});

export { api };
