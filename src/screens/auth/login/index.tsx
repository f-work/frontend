import { GoogleLogin } from "react-google-login";

export default function LoginScreen() {
  const handleSuccess = (res: any) => {
    console.log("login_success: ", res);
  };

  const handleFailure = (res: any) => {
    console.log("login_failed: ", res);
  };

  return (
    <div className="container-fluid bg-light text-dark py-4">
      <div className="row justify-content-center align-items-center">
        <h2 className="text-center">Login</h2>
      </div>
      <hr />
      <div
        className="d-flex justify-content-center align-items-center "
        style={{ height: "50vh" }}
      >
        <GoogleLogin
          clientId="672044993362-qaqf2o5d85sh59iia0ggmet7h9ilfpa3.apps.googleusercontent.com"
          buttonText="Google Login"
          onSuccess={handleSuccess}
          onFailure={handleFailure}
        />
      </div>
    </div>
  );
}
