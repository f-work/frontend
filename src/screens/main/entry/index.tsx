import GoogleLogin from "react-google-login";
import { Typeahead } from "react-bootstrap-typeahead";
import "react-bootstrap-typeahead/css/Typeahead.css";
import { useEffect, useState } from "react";
import { useSearchContext } from "context/search-context";
import { useNavigate } from "react-router";
import { routeConfig } from "utils/route-constants";
import { api } from "api";

const userToken = window.localStorage.getItem("token");

export default function EntryScreen() {
  const [isLoading, setIsLoading] = useState(false);
  const [suburbsLoading, setSuburbsLoading] = useState(false);
  const [suburbs, setSuburbs] = useState([]);
  const searchCtx = useSearchContext();
  const [suburb, setSuburb] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    setSuburbsLoading(true);
    api.get("/suburbs").then((res) => {
      const suburbs = res.data.suburbs.map((data: any) => {
        return data.locality + ", " + data.state + " " + data.postcode;
      });
      setSuburbs(suburbs);
      setSuburbsLoading(false);
    });
  }, []);

  const handleSuccess = async (res: any) => {
    console.log("res: ", res);
    console.log("tokenId: ", res.tokenId);

    setIsLoading(true);

    try {
      const response = await api.post("/google-login", {
        tokenId: res.tokenId,
      });
      const payload = response.data;
      const { token, user } = payload;
      window.localStorage.setItem("token", token);
      window.localStorage.setItem("user", JSON.stringify(user));
      window.location.href = "/user/profile";
    } catch (err) {
      console.log("error while login: ", err);
      setIsLoading(false);
    }
  };

  const handleFailure = (res: any) => {
    console.log("login_failed: ", res);
  };

  const handleSelected = (selected: any) => {
    const suburb = selected[0];
    setSuburb(suburb);
  };

  const handleClick = () => {
    searchCtx.onChangeSearch(suburb);
    navigate("/search");
  };

  return (
    <>
      {!userToken && (
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            marginTop: "20px",
            marginRight: "20px",
          }}
        >
          {isLoading && <div className="spinner-border" />}
          {!isLoading && (
            <GoogleLogin
              clientId="672044993362-qaqf2o5d85sh59iia0ggmet7h9ilfpa3.apps.googleusercontent.com"
              buttonText="Google Login"
              onSuccess={handleSuccess}
              onFailure={handleFailure}
            />
          )}
        </div>
      )}
      <div
        style={{
          width: "100%",
          height: "80vh",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div className="row" style={{ width: "30%" }}>
          <div className="col-md-12">
            {!userToken && (
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  marginBottom: "30px",
                }}
              ></div>
            )}

            <div className="form-group">
              {suburbsLoading && <div className="spinner-border" />}
              {!suburbsLoading && (
                <>
                  <Typeahead
                    id="suburb"
                    placeholder="Search Suburbs ...."
                    onChange={handleSelected}
                    options={suburbs}
                  />
                  <div
                    className="btn btn-success btn-sm mt-2 d-flex justify-content-center"
                    style={{ display: "flex", justifyContent: "center" }}
                    onClick={handleClick}
                  >
                    Search
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
