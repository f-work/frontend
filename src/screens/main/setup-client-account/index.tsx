export default function SetupClientAccountScreen() {
  return (
    <>
      <h3 className="text-center mt-2">Setup Client Account</h3>
      <div className="mt-2">
        <div className="row mt-4">
          <div className="col-md-5 offset-md-3">
            <div className="form-group">
              <label htmlFor="username">Username</label>
              <input type="text" className="form-control" id="username" />
            </div>
            <div className="form-group mt-3">
              <label htmlFor="email">Email</label>
              <input type="email" className="form-control" id="email" />
            </div>
            <button className="btn btn-primary mt-3">Create</button>
          </div>
        </div>
      </div>
    </>
  );
}
