import { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { Card, ListGroup } from "react-bootstrap";
import { routeConfig } from "utils/route-constants";
import BaseLayout from "layouts/base";
import { useSearchContext } from "context/search-context";
import { Typeahead } from "react-bootstrap-typeahead";
import { api } from "api";

export default function HomeScreen() {
  const [suburbsLoading, setSuburbsLoading] = useState(false);
  const [suburbs, setSuburbs] = useState([]);
  const [suburb, setSuburb] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [profiles, setProfiles] = useState<any>([]);
  const searchCtx = useSearchContext();

  useEffect(() => {
    setIsLoading(true);

    api.post("/filter/suburbs", { suburb: searchCtx.searchVal }).then((res) => {
      console.log("filter_suburbs: ", res.data.suburbs);
      setProfiles(res.data.suburbs);
      setIsLoading(false);
    });

    setSuburbsLoading(true);
    api.get("/suburbs").then((res) => {
      const suburbs = res.data.suburbs.map((data: any) => data.locality);
      setSuburbs(suburbs);
      setSuburbsLoading(false);
    });
  }, [searchCtx.searchVal]);

  return (
    <BaseLayout>
      {/* <div className="row">
        <div className="col-md-10">
          {suburbsLoading && <div className="spinner-border" />}
          {!suburbsLoading && (
            <Typeahead
              id="suburb"
              placeholder="Search Suburbs ...."
              options={suburbs}
              defaultInputValue={searchCtx.searchVal}
              onChange={(selected: any) => setSuburb(selected[0])}
            />
          )}
        </div>
        <div className="col-md-2">
          <button
            className="btn btn-success btn-md"
            onClick={() => searchCtx.onChangeSearch(suburb)}
          >
            Search
          </button>
        </div>
      </div> */}
      <div className="d-flex justify-content-center align-items-end mt-3">
        <p style={{ fontWeight: "bold", fontSize: "22px" }}>Search Results:</p>
        <p style={{ fontSize: "22px", marginLeft: "10px" }}>
          {searchCtx.searchVal}
        </p>
      </div>
      <hr />
      <div className="mb-5">
        <div className="row">
          {isLoading && <div className="spinner-border" />}
          {!isLoading && (
            <>
              {profiles.map((profile: any) => (
                <div className="col-md-4 my-3">
                  <SearchResultItem profile={profile} />
                </div>
              ))}
              {profiles.length === 0 && (
                <p className="alert alert-info">No results found ...</p>
              )}
            </>
          )}
        </div>
      </div>
    </BaseLayout>
  );
}

function SearchResultItem({ profile }: any) {
  const [isOpen, setIsOpen] = useState(false);
  const navigate = useNavigate();

  return (
    <Card>
      <Card.Header>
        <b>Name</b>: {profile.name}
      </Card.Header>
      <ListGroup>
        <ListGroup.Item>
          <b>Gender</b>: {profile.gender}
        </ListGroup.Item>
        <ListGroup.Item>
          <b>Driving School Name</b>: {profile.drivingSchoolName}
        </ListGroup.Item>
        <ListGroup.Item>
          <b>Email</b>: {profile.email}
        </ListGroup.Item>
        <ListGroup.Item>
          <b>Contact Number</b>: {profile.contactNo}
        </ListGroup.Item>
        <ListGroup.Item>
          <b>Hourly Rate</b>: ${profile.pricePerHour}
        </ListGroup.Item>
      </ListGroup>
      <Card.Body>
        <h4
          className="text-center pointer"
          onClick={() => setIsOpen((prev) => !prev)}
        >
          Packages
        </h4>
        {isOpen && <hr />}
        {isOpen && (
          <Card>
            <ListGroup>
              <ListGroup.Item>
                <b>Hours</b>: {profile.firstPackageHour}
              </ListGroup.Item>
              <ListGroup.Item>
                <b>Price</b>: ${profile.firstPackagePrice}
              </ListGroup.Item>
            </ListGroup>
            <ListGroup>
              <ListGroup.Item>
                <b>Hours</b>: {profile.secondPackageHour}
              </ListGroup.Item>
              <ListGroup.Item>
                <b>Price</b>: ${profile.secondPackagePrice}
              </ListGroup.Item>
            </ListGroup>
            <ListGroup>
              <ListGroup.Item>
                <b>Hours</b>: {profile.thirdPackageHour}
              </ListGroup.Item>
              <ListGroup.Item>
                <b>Price</b>: ${profile.thirdPackagePrice}
              </ListGroup.Item>
            </ListGroup>
          </Card>
        )}
      </Card.Body>
      <Card.Footer>
        <div>
          <a href={`tel:${profile.contactNo}`}>
            <i className="fa fa-phone pointer search-icon" />
          </a>
          <a href={`https://api.whatsapp.com/send?phone=${profile.contactNo}`}>
            <i className="fa fa-whatsapp mx-3 pointer search-icon" />
          </a>
          <span
            className="pointer"
            onClick={() =>
              navigate(routeConfig.bookNow.getRoutePath(profile._id))
            }
          >
            Book Now
          </span>
        </div>
      </Card.Footer>
    </Card>
  );
}
