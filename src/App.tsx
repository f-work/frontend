import { SearchContextProvider } from "context/search-context";
import AppRoutes from "./routes";

function App() {
  return (
    <>
      <SearchContextProvider>
        <AppRoutes />
      </SearchContextProvider>
    </>
  );
}

export default App;
