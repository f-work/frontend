import { Link } from "react-router-dom";
import { Container, Nav, Navbar } from "react-bootstrap";
import { routeConfig } from "utils/route-constants";

const userToken = window.localStorage.getItem("token");

export default function AppNavbar() {
  const handleLogout = () => {
    window.localStorage.clear();
    window.location.href = "/";
  };

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand href="#" as={Link} to="/">
          Student Tutor App
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
          >
            {userToken && (
              <>
                <Nav.Link as={Link} to={routeConfig.editProfile}>
                  Profile
                </Nav.Link>
                <Nav.Link onClick={handleLogout}>Logout</Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
