# React Typescript Boilerplate Code

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## What Includes:

- bootstrap v5.x (using react-bootstrap)
- basic routes setup using react-router-dom v6.x
- login screen

